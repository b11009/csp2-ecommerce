const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
});

router.post("/login" , (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});

router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	let userData = auth.decode(req.headers.authorization).id;

	// Provides the user's ID for the getProfile controller method
	userController.getProfile(userData).then(result => res.send(result));

});

router.get("/all", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllRegisteredUsers(isAdmin).then(result => res.send (result));
});

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.setAsAdmin(isAdmin, req.params, req.body).then(result => res.send(result));
});

router.post("/addToCart", auth.verify, (req, res) => {
	let data = {
			nonAdminUserId : auth.decode(req.headers.authorization).id,
			isAdmin : auth.decode(req.headers.authorization).isAdmin
		}	
	userController.addToCart(data, req.body).then(result => res.send(result));
});

router.get("/myCart", auth.verify, (req, res) => {
	let user = auth.decode(req.headers.authorization);
	console.log('getting my card ', user._id)
	userController.getUsersCart(user).then(result => res.send(result));
});

router.post("/checkout", auth.verify, (req, res) => {
	let user = auth.decode(req.headers.authorization);
	userController.checkoutCart(user , req.body).then(result => res.send(result));
});

router.get("/orders", auth.verify, (req,res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllOrders(isAdmin).then(result => res.send(result));
});

router.get("/hotproducts", (req,res) => {
	userController.getHotProducts().then(result => res.send(result));
});

router.get("/myOrders", auth.verify, (req,res) => {
	let user = auth.decode(req.headers.authorization);
	userController.getUsersOrders(user).then(result => res.send(result));
});


module.exports = router;