const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProduct(data).then(result => res.send(result));
});

router.get("/", (req, res) => {
	productController.getAllActiveProducts().then(result => res.send(result));
})

router.get("/all", (req, res) => {
	productController.getAllProducts().then(result => res.send(result));
})

router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(result => res.send(result));
})

router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(isAdmin, req.params, req.body).then(result => res.send(result));
})

router.put("/:productId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(isAdmin, req.params).then(result => res.send(result));
})

router.put("/:productId/restore", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.restoreProduct(isAdmin, req.params).then(result => res.send(result));
})


module.exports = router;