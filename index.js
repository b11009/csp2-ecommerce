const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

mongoose.connect("mongodb+srv://jeniezuitt:admin@cluster0.vsejk.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology: true
});
mongoose.connection.once('open', () => console.log ('Now connected to MongoDB Atlas'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// routes configuration
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const setAsAdminRoutes = require("./routes/user");
app.use("/",setAsAdminRoutes);
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});