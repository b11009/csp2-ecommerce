const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	items: [
		{
			productId: {
				type: String,
				required: [true, "ProductId is required"]
			},
			quantity: {
				type: Number,
				default: 0
			},
			price: {
				type: Number,
				default: 0
			},
			productName: {
				type: String
			}
		}
	],
	userId: {
		type: String,
		required: [true, "UserId is required"]
	},
	totalQuantity: {
		type: Number,
		default: 0,
		required: [true, "Total amount is required"]
	},
	totalAmount: {
		type: Number,
		default: 0,
		required: [true, "Total amount is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Cart", cartSchema);