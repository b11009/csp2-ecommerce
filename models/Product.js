const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	category: {
		type: String,
		required: [true, "Category is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	imgSrc: {
		type: String,
		default: "https://jeniespetstore.s3.ap-southeast-1.amazonaws.com/noimage.jpeg"
	}
});

module.exports = mongoose.model("Product",productSchema);