const mongoose = require("mongoose");

const checkoutSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId is required"]
	},
	address: {
	    type: String,
	    required: true
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	order: {
		totalQuantity: {
			type: Number,
			default: 0,
			required: true
		},
		totalAmount: {
			type: Number,
			default: 0,
			required: true
		},
		items: [{
			productId: {
				type: String,
				required: [true, "ProductId is required"]
			},
			quantity: {
				type: Number,
				default: 0
			},
			price: {
				type: Number,
				default: 0
			},
			productName: {
				type: String
			}
		}]
	}
})
	
module.exports = mongoose.model("CheckoutOrder",checkoutSchema);
