const Product = require("../models/Product");
const auth = require("../auth");

module.exports.createProduct = (data) => {
	if (!data.isAdmin) {
			return Promise.resolve("Admin access is required to perform this operation.");
	} 

	return Product.find({name : data.product.name}).then(result => {
		if (result.length > 0) {
			return {message:"Product is already in the list, please use PUT request to make update"};
		}

		let newProduct = new Product ({
			    name: data.product.name,
			    description: data.product.description,
			    price: data.product.price,
			    category: data.product.category,
			    imgSrc: data.product.imgSrc
			});

		return newProduct.save().then((product, error) => {
	    	if (error) {
	      		return false;
	    	} else {
	      		return true;
	    	}
	  	})
	})
}

module.exports.getAllActiveProducts = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (isAdmin, reqParams , reqBody) => {
	if(!isAdmin){
		return Promise.resolve("Admin access is required to perform this operation.")
	}

	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		category : reqBody.category,
		imgSrc : reqBody.imgSrc
	}
	return Product.findByIdAndUpdate(reqParams.productId , updatedProduct).then((oldProductDetails, error) => {
		if(error){
			return false;
		} else {
			return { 
			original : oldProductDetails,
				updated : {
					name : reqBody.name ? reqBody.name : oldProductDetails.name,
					description : reqBody.description ? reqBody.description : oldProductDetails.description,
					price : reqBody.price ? reqBody.price : oldProductDetails.price,
					category : reqBody.category ? reqBody.category : oldProductDetails.category,
					imgSrc : reqBody.imgSrc ? reqBody.imgSrc : oldProductDetails.imgSrc,
					isActive : typeof reqBody.isActive === "boolean" ? reqBody.isActive : oldProductDetails.isActive
				}
			};
		}
	})
}

module.exports.archiveProduct = (isAdmin, reqParams) => {
	if(!isAdmin){
		return Promise.resolve("Admin access is required to perform this operation.")
	}

	let updateIsActive = {
		isActive : false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateIsActive).then((product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.restoreProduct = (isAdmin, reqParams) => {
	if(!isAdmin){
		return Promise.resolve("Admin access is required to perform this operation.")
	}

	let updateIsActive = {
		isActive : true
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateIsActive).then((product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}




