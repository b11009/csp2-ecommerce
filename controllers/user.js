const User = require("../models/User");
const Cart = require("../models/Cart");
const Product = require("../models/Product");
const CheckoutOrder = require("../models/CheckoutOrder");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration, Authentication, Set as Admin
module.exports.registerUser = (reqBody) => { 
	if(!reqBody.email || !reqBody.password || !reqBody.firstName || !reqBody.lastName || !reqBody.mobileNo){
		return Promise.resolve("Please input complete details.");
	}
	return User.find({email:reqBody.email}).then(result => {
		if (result.length > 0){
			return {message:"Email Exists"};
		} else {
			let newUser = new User ({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				mobileNo : reqBody.mobileNo,
				email : reqBody.email,
				password : bcrypt.hashSync(reqBody.password, 10)
			});
			return newUser.save().then((user, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if (!result){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return {access : auth.createAccessToken(result)}
			} else {
				return {message:"Incorrect Password"};
			}
		}
	})
}

module.exports.getProfile = (userData) => {
	return User.findById(userData).then(result => {	
		console.log(userData)
		result.password = "";
		return result;
	});
};


module.exports.getAllRegisteredUsers = (isAdmin) => {
	if(!isAdmin){
		return Promise.resolve("Admin access is required to perform this operation.")
	}
	return User.find({}).then(users => {
		return users.map((user) => {
			user.password = "";
			return user	
		})
	})
}

module.exports.setAsAdmin = async (isAdmin, reqParams, reqBody) => {
	if(!isAdmin){
		return ("Admin access is required to perform this operation.");
	}

	let setUserAsAdmin = {
		isAdmin: true
	};
	try {
		return await User.findByIdAndUpdate(reqParams.userId, setUserAsAdmin).then((user) => {
	  		if (user.isAdmin) {
	      		return (`"${user.email}" is already an admin.`);
	    	} else {
		      	return (`"${user.email}" has been set as admin.`);
	    	}
		})
	} catch {
	  	return Promise.resolve("Invalid UserId");
	}
}

// Create Order, Retrieve Orders
module.exports.addToCart = (data, reqBody) => {
	if (data.isAdmin) {
			return Promise.resolve("Admin is not allowed to perform this operation.");
	} 

	const cart = reqBody

	return Product.find({
		$or: 
			cart.map(product => { 
				return { _id: product.productId } 
			})
		}).then(products => { 

		products = products.filter(product => product.isActive)

		const totalAmount = products.reduce((sum, product) => {
			return sum + product.price * cart.find(cartItem => cartItem.productId == product._id).quantity
			}, 0);

		const totalQuantity = products.reduce((total, product ) => {
			return total + cart.find(cartItem => cartItem.productId == product._id).quantity
			}, 0);

		return Cart.findOne({userId : data.nonAdminUserId}).then(result => {
			if (!result){
				let newCart = new Cart ({
					userId : data.nonAdminUserId,
					totalAmount : totalAmount,
					totalQuantity : totalQuantity,
					items : 
						products.map((product,i) => {
							return {
							productId : product._id,
							quantity : cart.find(cartItem => cartItem.productId == product._id).quantity,
							price : product.price,
							productName : product.name
							}
						})
					})

				return newCart.save().then((cart, error) => {
					if (error) {
						return false;
					} else {
						return cart;
					}
				})
			} else {
				const itemCartIds = result.items.map(item => item.productId)
				products.forEach(product => {
					const currentProductId = product._id.toString()
					const indexOfItem = itemCartIds.indexOf(currentProductId)
					if (indexOfItem > -1){

						result.items[indexOfItem].quantity += cart.find(cartItem => cartItem.productId == currentProductId).quantity	
					}else{
						result.items.push({
								productId : currentProductId,
								quantity : cart.find(cartItem => cartItem.productId == currentProductId).quantity,
								price : product.price,
								productName : product.name
						})
					}
				})
				const updatedCart = result
				// removes zero or negative quantity in cart
				updatedCart.items = updatedCart.items.filter(item => item.quantity > 0)
				const newtotalAmount = updatedCart.items.reduce((sum, product) => {
					return sum + product.price * product.quantity
					}, 0);

				const newtotalQuantity = updatedCart.items.reduce((total, product ) => {
					return total + product.quantity
					}, 0);

				updatedCart.totalAmount = newtotalAmount
				updatedCart.totalQuantity = newtotalQuantity

				return Cart.findByIdAndUpdate(result._id,updatedCart,{overwrite : false}).then((oldCart)=>{
					return {oldCart: oldCart, newCart: updatedCart}
				});
			}
		})

		
	});
}

module.exports.getUsersCart = (user, reqBody) => {
	if(user.isAdmin){
		return Promise.resolve("Admin is not allowed to perform this operation.");
	}
	console.log(user)
	return Cart.findOne({userId : user.id}).then(result => {
		if(result){
			return result;
		} else {
			return false;
		}
	})

};

module.exports.checkoutCart = (user, reqBody) => {
	console.log(user)
	if (user.isAdmin) {
		return Promise.resolve("Admin is not allowed to perform this operation.");
	} 

	const {cart, address} = reqBody
	// get products
	return Product.find({
		$or: 
			cart.map(product => { 
				return { _id: product.productId } 
			})
		}).then(products => { 
		const totalAmount = products.reduce((sum, product) => {
			return sum + product.price * cart.find(cartItem => cartItem.productId == product._id).quantity
			}, 0);

		const totalQuantity = products.reduce((total, product ) => {
			return total + cart.find(cartItem => cartItem.productId == product._id).quantity
			}, 0);

		//create new checkoutcart object which will be output to order
		let checkoutCart = new CheckoutOrder ({
					userId : user.id,
					address: address ? address : "Default Address",
					order: {
						totalAmount : totalAmount,
						totalQuantity : totalQuantity,
						items : 
						products.map((product,i) => {
							return {
							productId : product._id,
							quantity : cart.find(cartItem => cartItem.productId == product._id).quantity,
							price : product.price,
							productName : product.name
						}
				})
			}
		})	

		return Cart.findOne({userId : user.id}).then(existingCart => {
			// code for decrementing quantity in existing cart
			if (existingCart){
				const itemCartIds = existingCart.items.map(item => item.productId)
				products.forEach(product => {
					const currentProductId = product._id.toString()
					const indexOfItem = itemCartIds.indexOf(currentProductId)
					if (indexOfItem > -1){

						existingCart.items[indexOfItem].quantity -= cart.find(cartItem => cartItem.productId == currentProductId).quantity	
					}
				})

				const updatedCart = existingCart
				// removes items with negative or zero quantity
				updatedCart.items = updatedCart.items.filter(item => item.quantity > 0)
				// recompute new total amount and quantity
				const newtotalAmount = updatedCart.items.reduce((sum, product) => {
					return sum + product.price * product.quantity
					}, 0);

				const newtotalQuantity = updatedCart.items.reduce((total, product ) => {
					return total + product.quantity
					}, 0);

				updatedCart.totalAmount = newtotalAmount
				updatedCart.totalQuantity = newtotalQuantity

				return Cart.findByIdAndUpdate(existingCart._id,updatedCart,{overwrite : false}).then((oldCart)=>{
					return checkoutCart.save().then((checkedOutCart, error) => {
						if (error) {
							return false;
						} else {
							return {oldCart: oldCart, newCart: updatedCart, order: checkoutCart}
						}
					})
				})
			}else{

					return checkoutCart.save().then((checkedOutCart, error) => {
						if (error) {
							return false;
						} else {
							return {order: checkoutCart}
						}
					})
			}
		})
	})
}

module.exports.getAllOrders = (isAdmin) => {
	if(!isAdmin){
		return {message:"Admin access is required to perform this operation."};
	}
	return CheckoutOrder.find({}).then(result => {
		return result;
	})
};


module.exports.getHotProducts = () => {
	console.log("getHotProducts")
	return CheckoutOrder.find({}).then(result => {

		const orderCount = []

		result.map(({ order }) => { return order.items }).flat().forEach((order, i) => {

		  const existingOrder = orderCount.findIndex(eo => eo.productId == order.productId)
		  console.log(i)
		  console.log({ existingOrder })
		  if (existingOrder > -1) {
		    orderCount[existingOrder].quantity = orderCount[existingOrder].quantity + order.quantity
		  } else {
		    orderCount.push(order)
		  }
		})

		return orderCount.sort((a,b) => b.quantity-a.quantity);
	})
};

module.exports.getUsersOrders = (user, reqBody) => {
	if(user.isAdmin){
		return {message:"Admin is not allowed to perform this operation."};
	}
	return CheckoutOrder.find({userId : user.id}).then(result => {
			return result;
	})

};





